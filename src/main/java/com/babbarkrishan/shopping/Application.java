package com.babbarkrishan.shopping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.babbarkrishan.shopping.entity.Product;
import com.babbarkrishan.shopping.repository.ProductRepository;

/**
 * Hello world!
 *
 */

//OR
/*@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@ComponentScan(basePackages = {"com.babbarkrishan.shopping"})*/
@SpringBootApplication
public class Application {

/*@SpringBootApplication
public class Application {*/
	
	@Autowired
	private ProductRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/*public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}*/
	
	//@Override
	public void run(String... args) throws Exception {

		repository.deleteAll();

		// save a couple of products
		repository.save(new Product("Hard Disk 1 TB", "Hard Disk 1 TB", 3076.00f, "D:/test/one.jpg"));
		repository.save(new Product("Hard Disk 2 TB", "Hard Disk 2 TB", 5276.00f, "D:/test/one.jpg"));

		// fetch all products
		System.out.println("Products found with findAll():");
		System.out.println("-------------------------------");
		for (Product product : repository.findAll()) {
			System.out.println(product);
		}
		System.out.println();

		// fetch an individual product
		System.out.println("Product found with findByFirstName('Alice'):");
		System.out.println("--------------------------------");
		System.out.println(repository.findByProductName("Hard Disk 1 TB"));

		/*System.out.println("Products found with findByLastName('Smith'):");
		System.out.println("--------------------------------");
		for (Product product : repository.findByLastName("Smith")) {
			System.out.println(product);
		}*/

	}
}
