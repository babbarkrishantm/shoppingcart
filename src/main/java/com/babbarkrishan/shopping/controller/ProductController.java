package com.babbarkrishan.shopping.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.babbarkrishan.shopping.entity.Product;
import com.babbarkrishan.shopping.exception.ProductNotFoundException;
import com.babbarkrishan.shopping.service.ProductService;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	private final ProductService service;

	@Autowired
    ProductController(ProductService service) {
        this.service = service;
    }

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	Product create(@RequestBody @Valid Product product) {
		return service.create(product);
	}

	/*@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	ProductDTO delete(@PathVariable("id") String id) {
		return service.delete(id);
	}*/

	@RequestMapping(method = RequestMethod.GET)
	List<Product> findAll() {
		return service.findAll();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	Product findById(@PathVariable("id") String id) throws ProductNotFoundException {
		return service.findById(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	Product update(@RequestBody @Valid Product productEntry) throws ProductNotFoundException {
		return service.update(productEntry);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleProductNotFound(ProductNotFoundException ex) {
	}
}