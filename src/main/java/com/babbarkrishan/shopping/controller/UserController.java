package com.babbarkrishan.shopping.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.babbarkrishan.shopping.bean.UserSearchCriteria;
import com.babbarkrishan.shopping.entity.User;
import com.babbarkrishan.shopping.exception.UserNotFoundException;
import com.babbarkrishan.shopping.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	private final UserService service;

	@Autowired
	UserController(UserService service) {
        this.service = service;
    }

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	User create(@RequestBody @Valid User user) {
		return service.create(user);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	User findById(@PathVariable("id") String id) throws UserNotFoundException {
		return service.findById(id);
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	List<User> findByEmailId(@RequestBody @Valid UserSearchCriteria userSearchCriteria) throws UserNotFoundException {
		return service.search(userSearchCriteria);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	User update(@RequestBody @Valid User user) throws UserNotFoundException {
		return service.update(user);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleUserNotFound(UserNotFoundException ex) {
	}
}