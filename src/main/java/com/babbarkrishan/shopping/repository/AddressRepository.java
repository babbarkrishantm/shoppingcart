
package com.babbarkrishan.shopping.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.babbarkrishan.shopping.entity.Address;

public interface AddressRepository extends MongoRepository<Address, String> {

	//Product findByEmailId(String emailId);
}
