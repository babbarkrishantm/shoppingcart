
package com.babbarkrishan.shopping.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.babbarkrishan.shopping.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmailId(String emailId);
}
