
package com.babbarkrishan.shopping.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.babbarkrishan.shopping.entity.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

	Product findByProductName(String productName);
}
