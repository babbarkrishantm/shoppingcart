package com.babbarkrishan.shopping.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.babbarkrishan.shopping.bean.UserSearchCriteria;
import com.babbarkrishan.shopping.entity.User;
import com.babbarkrishan.shopping.exception.UserNotFoundException;
import com.babbarkrishan.shopping.repository.UserRepository;

@Service
final class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User create(User user) {		
		return userRepository.save(user);
		
	}

	@Override
	public List<User> findAll() {
		List<User> userEntries = userRepository.findAll();
		return userEntries;
	}

	@Override
	public User findById(String id) throws UserNotFoundException {
		User found = findUserById(id);
		return found;
	}
	
	@Override
	public List<User> search(UserSearchCriteria userSearchCriteria) throws UserNotFoundException {
		List<User> users = new ArrayList<User>();
		if (StringUtils.isNotEmpty(userSearchCriteria.getEmailId())) {
			User result = userRepository.findByEmailId(userSearchCriteria.getEmailId());
			users.add(result);
		}
		return users;
	}

	@Override
	public User update(User user) throws UserNotFoundException {
		User updated = findUserById(user.getId().toString());
		/*updated.setUserName(user.getUserName());
		updated.setPrice(user.getPrice());
		updated.setDescription(user.getDescription());
		updated.setImagePath(user.getImagePath());*/
		updated = userRepository.save(updated);
		return updated;
	}

	private User findUserById(String id) throws UserNotFoundException {
		User result = userRepository.findOne(id);
		return result;
	}
}