package com.babbarkrishan.shopping.service;

import java.util.List;

import com.babbarkrishan.shopping.entity.Product;
import com.babbarkrishan.shopping.exception.ProductNotFoundException;

public interface ProductService {
	Product create(Product product);

	List<Product> findAll();

	Product findById(String id) throws ProductNotFoundException;

	Product update(Product product) throws ProductNotFoundException;
}
