package com.babbarkrishan.shopping.bean;

public class UserSearchCriteria {	
	private String emailId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}	
}
