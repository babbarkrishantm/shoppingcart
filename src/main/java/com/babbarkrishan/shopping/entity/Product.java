
package com.babbarkrishan.shopping.entity;


import org.springframework.data.annotation.Id;

//@Document(collection = "product")
public class Product {
	@Id
	private String id;

	private String productName;
	private String description;
	private float price;
	private String imagePath;

	public Product() {		
	}
	
	public Product(String productName, String description, float price, String imagePath) {
		this.productName = productName;
		this.description = description;
		this.price= price;
		this.imagePath = imagePath; 
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String toString() {
		return "Product [Id=" + id + ", productName=" + productName + ", description=" + description
				+ ", price=" + price + ", imagePath=" + imagePath + "]";
	}
}