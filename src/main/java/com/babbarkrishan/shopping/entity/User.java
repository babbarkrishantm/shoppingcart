package com.babbarkrishan.shopping.entity;


import java.util.List;

import org.springframework.data.annotation.Id;

public class User {
	@Id
	private String id;
	
	private String emailId;

	private List<Order> orders;
	private List<Address> addresses;
	
	public User() {		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public List<Address> getAddresses() {
		return addresses;
	}
	
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return "User [Id=" + id + ", emailId=" + emailId + ", orders=" + orders.toString() + ", addresses=" + addresses.toString() + "]";
	}	
}