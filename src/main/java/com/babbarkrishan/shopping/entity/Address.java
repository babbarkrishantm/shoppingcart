package com.babbarkrishan.shopping.entity;

import org.springframework.data.annotation.Id;

import com.babbarkrishan.shopping.Constants.AddressType;

public class Address {
	
	@Id
	private String id;
	 
	private String fullName; 
	private String houseNumber; 
	private String address1;
	private String address2; 
	private String city; 
	private String state; 
	private String pinCode; 
	private String mobileNumber; 
	private AddressType addressType;
	
	public Address() {		
	}
	
	public Address(String fullName, String houseNumber, String address1, String address2, String city, String state, 
			String pinCode, String mobileNumber, AddressType addressType) {
		this.fullName = fullName;
		this.houseNumber = houseNumber;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.pinCode = pinCode;
		this.mobileNumber = mobileNumber;
		this.addressType = addressType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public AddressType getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}
	

	@Override
	public String toString() {
		return "Address [Id=" + id + ", fullName=" + fullName + ", houseNumber=" + houseNumber
				+ ", address1=" + address1 +  ", address2=" + address2 +  ", city=" + city
				+ ", state=" + state +  ", pinCode=" + pinCode +  ", mobileNumber=" + mobileNumber +  ", addressType=" + addressType.getValue() + "]";
	}
}